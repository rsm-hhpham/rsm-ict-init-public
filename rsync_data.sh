#!/bin/bash

## sync ICT2019 folder from Dropbox to your home directory on the MSBA server
## replace 'vnijs' with your ucsd user id
rsync -azvh -e ssh ~/Dropbox/ICT2019 vnijs@rsm-compute-01.ucsd.edu:~;
